package faust.providence.app.lessonexercise

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_a2.*

class A2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a2)

        val stringGot = this.intent.extras.getString(MainActivity.INPUT_PAR)
        this.button.text = stringGot
    }

    fun backToPraient(view: View) {
        val intent= Intent()
        intent.putExtra("back","button click")
        this.setResult(Activity.RESULT_OK, intent)
        this.finish()
    }
}
