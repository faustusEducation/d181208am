package faust.providence.app.lessonexercise

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        val INPUT_PAR = "input"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun goA2(view: View) {
        val intent = Intent(this,A2Activity::class.java )
        intent.putExtra(INPUT_PAR, "Just For Test!")
        this.startActivityForResult(intent, 1)
    }

    fun goList(view: View) {
        val intent = Intent(this,ListActivity::class.java)
        this.startActivity(intent)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        this.textView.text = data?.extras?.getString("back")
    }
}
