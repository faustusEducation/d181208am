package faust.providence.app.lessonexercise

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Adapter
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_list.*

class ListActivity : AppCompatActivity() {

    companion object {
        val fruits = arrayOf("apple","banana","oriange")
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        val myAdpater = ArrayAdapter(this,android.R.layout.simple_list_item_1,fruits)
        this.listView.adapter = myAdpater

    }
}
